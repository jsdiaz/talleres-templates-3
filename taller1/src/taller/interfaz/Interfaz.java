package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	private static final String MAQUINA = "Máquina"; 

	/**
	 * Juego actual
	 */
	private LineaCuatro juego;

	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;

	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
	}

	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
		System.out.println("¿Cuántos jugadores?");
		int n = sc.nextInt();
		ArrayList<Jugador> jugadores = new ArrayList<>(n);
		for(int i = 0; i < n; i++){
			System.out.println("¿Cuál es el nombre del jugador " + (i+1) + "?");
			String nombre = sc.next();
			System.out.println("¿Qué símbolo desea usar?");
			String simbolo = sc.next();
			jugadores.add(new Jugador(nombre, " " + simbolo + " "));
		}
		System.out.println("¿De qué tamaño desean el tablero? (escriba las dos dimensiones separadas por un espacio)");
		juego = new LineaCuatro(jugadores, sc.nextInt(), sc.nextInt());
		juego();
	}

	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		while(!juego.fin()){
			System.out.println("¿En que columna desea jugar, " + juego.darAtacante() + "?");
			if(juego.registrarJugada(sc.nextInt() - 1))
				imprimirTablero();
			else
				System.out.println("La columna escogida está llena.");
		}
		System.out.println("¡Felicitaciones! " + juego.darAtacante() + " ha ganado el juego.");
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		ArrayList<Jugador> jugadores = new ArrayList<>(2);
		System.out.println("¿Cuál es su nombre?");
		String nombre = sc.next();
		System.out.println("¿Qué símbolo desea usar?");
		String simbolo = sc.next();
		jugadores.add(new Jugador(nombre, " " + simbolo + " "));
		jugadores.add(new Jugador(MAQUINA, simbolo.equals("O")? " X " : " O "));

		System.out.println("¿De qué tamaño desea el tablero? (escriba las dos dimensiones separadas por un espacio)");
		juego = new LineaCuatro(jugadores, sc.nextInt(), sc.nextInt());
		juegoMaquina();
	}

	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		while(!juego.fin()){
			if(juego.darAtacante().equals(MAQUINA)){
				juego.registrarJugadaAleatoria();
				imprimirTablero();
			}	
			else{
				System.out.println("¿En que columna desea jugar?");
				if(!juego.registrarJugada(sc.nextInt() - 1))
					System.out.println("La columna escogida está llena.");
			}
		}
		if(juego.darAtacante().equals(MAQUINA))
			System.out.println("Lo sentimos, ha perdido el juego.");
		else
			System.out.println("Felicitaciones! Ha ganado el juego.");
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		String[][] tablero = juego.darTablero();
		String fila = "";
		for(int j = 0; j < tablero[0].length; j++)
			fila += " " + (j+1) + " ";
		for(int i = 0; i < tablero.length; i++){
			fila = "";
			for(int j = 0; j < tablero[0].length; j++)
				fila += tablero[i][j];
			System.out.println(fila);
		}
	}
}
