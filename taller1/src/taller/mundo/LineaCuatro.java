package taller.mundo;

import java.util.ArrayList;

public class LineaCuatro 
{
	/**
	 * Constante que muestra una casilla vacia.
	 */
	private static final String VACIO = "___";

	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;

	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;

	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;

	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;

	/**
	 * El número del jugador en turno
	 */
	private int turno;

	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]=VACIO;
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}

	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}

	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		while(!registrarJugada((int) (Math.random()*tablero[0].length)));
	}

	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		if(!tablero[0][col].equals(VACIO))
			return false;

		int fil = tablero.length - 1;
		while(!tablero[fil][col].equals(VACIO)) fil--;
		tablero[fil][col] = jugadores.get(turno).darSimbolo();

		if(!terminar(fil, col)){
			turno = (turno + 1) % jugadores.size();
			atacante = jugadores.get(turno).darNombre();
		}

		return true;
	}

	/**
	 * Determina si hay cuatro en linea en la fila o diagonal con la posición dada
	 * @param step 0 si se quiere ver la fila, 1 o -1 para las diagonales.
	 */
	private boolean ganoOtro(int fil, int col, String ficha, int step)
	{
		for(int i = -3; i <= 0; i++){
			int fil1 = fil + i, col1 = col + i*step;
			if(fil1 >= 0 && col1 >= 0 &&  col1 < tablero[0].length && tablero[fil1][col1].equals(ficha)){
				boolean flag = true;
				for(int j = 1; j < 4 && flag; j++){
					int fil2 = fil1 + j, col2 = col1 + j*step;
					if(fil2 >= tablero.length || col2 < 0 || col2 >= tablero[0].length || !tablero[fil2][col2].equals(ficha))
						flag = false;
				}
				if(flag) return true;
			}	
		}
		return false;
	}

	/**
	 * Determina si hay cuatro en linea en la columna con la posición dada
	 */
	private boolean ganoColumna(int fil, int col, String ficha)
	{
		for(int col2 = col - 1; col2 >= col - 3; col2--)
			if(col2 < 0 || !tablero[fil][col2].equals(ficha))
				return false;
		return true;
	}

	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{
		String ficha = tablero[fil][col];
		finJuego = ganoColumna(fil, col, ficha) || ganoOtro(fil, col, ficha, -1) 
				|| ganoOtro(fil, col, ficha, 0) || ganoOtro(fil, col, ficha, 1);
		return finJuego;
	}



}
